#!/usr/bin/env bash
for var in "User" "Hall" "Place" "Film" "Staff" "FilmStaff" "Review" "Rating" "Seance" "Ticket" "Assignment";
do
    docker exec -it cinema_php_1 php yii fixture $var
done
