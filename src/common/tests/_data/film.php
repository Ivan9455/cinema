<?php

return [
    [
        'title' => 'iron man 1',
        'description' => 'After being held captive in an 
        Afghan cave, billionaire engineer Tony Stark creates 
        a unique weaponized suit of armor to fight evil.',
        'time' => '02:16:00',
        'country' => 'USA',
        'release_date' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, 2001)),
        'img' => '1.jpg',
        'rating_kinopoisk' => 7.4,
        'rating_imdb' => 7.41,
        'rating_rotten_tomatoes' => 7.45
    ],
    [
        'title' => 'iron man 2',
        'description' => 'After being held captive in an 
        Afghan cave, billionaire engineer Tony Stark creates 
        a unique weaponized suit of armor to fight evil.',
        'time' => '02:16:00',
        'country' => 'USA',
        'release_date' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, 2002)),
        'img' => '1.jpg',
        'rating_kinopoisk' => 7.5,
        'rating_imdb' => 7.51,
        'rating_rotten_tomatoes' => 7.55
    ],
    [
        'title' => 'iron man 3',
        'description' => 'After being held captive in an 
        Afghan cave, billionaire engineer Tony Stark creates 
        a unique weaponized suit of armor to fight evil.',
        'time' => '02:16:00',
        'country' => 'USA',
        'release_date' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, 2003)),
        'img' => '1.jpg',
        'rating_kinopoisk' => 6.4,
        'rating_imdb' => 6.41,
        'rating_rotten_tomatoes' => 6.45
    ],
    [
        'title' => 'iron man 4',
        'description' => 'After being held captive in an 
        Afghan cave, billionaire engineer Tony Stark creates 
        a unique weaponized suit of armor to fight evil.',
        'time' => '02:16:00',
        'country' => 'USA',
        'release_date' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, 2004)),
        'img' => '1.jpg',
        'rating_kinopoisk' => 7.7,
        'rating_imdb' => 7.71,
        'rating_rotten_tomatoes' => 7.83
    ],
    [
        'title' => 'iron man 5',
        'description' => 'After being held captive in an 
        Afghan cave, billionaire engineer Tony Stark creates 
        a unique weaponized suit of armor to fight evil.',
        'time' => '02:16:00',
        'country' => 'USA',
        'release_date' => date('Y-m-d H:i:s', mktime(0, 0, 0, 1, 1, 2005)),
        'img' => '1.jpg',
        'rating_kinopoisk' => 8.33,
        'rating_imdb' => 7.21,
        'rating_rotten_tomatoes' => 9.45
    ]
];
