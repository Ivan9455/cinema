<?php
$result = [];
for ($i = 1; $i <= 16; $i++) {
    for ($j = 1; $j <= 10; $j++) {
        $result[] = [
            'hall_id' => 1,
            'row' => $i,
            'position' => $j,
            'rate' => 1
        ];
    }
}
for ($i = 1; $i <= 10; $i++) {
    for ($j = 1; $j <= 8; $j++) {
        $result[] = [
            'hall_id' => 2,
            'row' => $i,
            'position' => $j,
            'rate' => 1.2
        ];
    }
}
for ($i = 1; $i <= 2; $i++) {
    for ($j = 1; $j <= 3; $j++) {
        $result[] = [
            'hall_id' => 3,
            'row' => $i,
            'position' => $j,
            'rate' => 3
        ];
    }
}
return $result;
