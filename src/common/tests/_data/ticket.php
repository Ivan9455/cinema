<?php

return [
    [
        'user_id' => 1,
        'seance_id' => 1,
        'place_id' => 1,
        'status' => 1,
        'price' => 100,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 2,
        'seance_id' => 1,
        'place_id' => 2,
        'status' => 1,
        'price' => 100,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 3,
        'seance_id' => 1,
        'place_id' => 3,
        'status' => 1,
        'price' => 100,
        'token' => '',
        'ics_name' => 'test'
    ],

    [
        'user_id' => 3,
        'seance_id' => 2,
        'place_id' => 161,
        'status' => 1,
        'price' => 120,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 4,
        'seance_id' => 2,
        'place_id' => 162,
        'status' => 1,
        'price' => 120,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 5,
        'seance_id' => 2,
        'place_id' => 163,
        'status' => 1,
        'price' => 120,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 7,
        'seance_id' => 3,
        'place_id' => 241,
        'status' => 1,
        'price' => 300,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 8,
        'seance_id' => 3,
        'place_id' => 242,
        'status' => 1,
        'price' => 300,
        'token' => '',
        'ics_name' => 'test'
    ],
    [
        'user_id' => 9,
        'seance_id' => 3,
        'place_id' => 243,
        'status' => 1,
        'price' => 300,
        'token' => '',
        'ics_name' => 'test'
    ]
];
