<?php
return [
    [
        'review_id' => 1,
        'user_id' => 1,
        'value' => 5
    ],
    [
        'review_id' => 1,
        'user_id' => 2,
        'value' => 5
    ],
    [
        'review_id' => 1,
        'user_id' => 3,
        'value' => 5
    ],
    [
        'review_id' => 2,
        'user_id' => 4,
        'value' => 5
    ],
    [
        'review_id' => 2,
        'user_id' => 5,
        'value' => 4
    ],
    [
        'review_id' => 2,
        'user_id' => 6,
        'value' => 5
    ],
    [
        'review_id' => 3,
        'user_id' => 1,
        'value' => 3
    ],
    [
        'review_id' => 3,
        'user_id' => 2,
        'value' => 4
    ],
    [
        'review_id' => 3,
        'user_id' => 3,
        'value' => 5
    ],
    [
        'review_id' => 4,
        'user_id' => 1,
        'value' => 4
    ],
    [
        'review_id' => 4,
        'user_id' => 2,
        'value' => 4
    ],
    [
        'review_id' => 4,
        'user_id' => 3,
        'value' => 2
    ],
    [
        'review_id' => 5,
        'user_id' => 6,
        'value' => 4
    ],
    [
        'review_id' => 5,
        'user_id' => 2,
        'value' => 2
    ],
    [
        'review_id' => 5,
        'user_id' => 9,
        'value' => 2
    ]
];
