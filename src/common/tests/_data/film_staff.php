<?php
return [
    [
        'film_id' => '1',
        'staff_id' => '1',
        'role' => 'actor'
    ],
    [
        'film_id' => '1',
        'staff_id' => '2',
        'role' => 'screenplay'
    ],
    [
        'film_id' => '1',
        'staff_id' => '3',
        'role' => 'director'
    ],
    [
        'film_id' => '2',
        'staff_id' => '4',
        'role' => 'actor'
    ],
    [
        'film_id' => '2',
        'staff_id' => '5',
        'role' => 'screenplay'
    ],
    [
        'film_id' => '2',
        'staff_id' => '6',
        'role' => 'director'
    ],
    [
        'film_id' => '3',
        'staff_id' => '7',
        'role' => 'actor'
    ],
    [
        'film_id' => '3',
        'staff_id' => '8',
        'role' => 'screenplay'
    ],
    [
        'film_id' => '3',
        'staff_id' => '9',
        'role' => 'director'
    ],
    [
        'film_id' => '4',
        'staff_id' => '1',
        'role' => 'actor'
    ],
    [
        'film_id' => '4',
        'staff_id' => '2',
        'role' => 'screenplay'
    ],
    [
        'film_id' => '4',
        'staff_id' => '3',
        'role' => 'director'
    ],
    [
        'film_id' => '5',
        'staff_id' => '4',
        'role' => 'actor'
    ],
    [
        'film_id' => '5',
        'staff_id' => '5',
        'role' => 'screenplay'
    ],
    [
        'film_id' => '5',
        'staff_id' => '6',
        'role' => 'director'
    ]
];
