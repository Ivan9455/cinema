<?php
$text = 'The coelacanth (pronounced SEE-la-kanth) 
is the lone survivor of an ancient line of 
lobe fin fishes thought to have been extinct 
for 60 to 70 million years. Their rediscovery,
 in 1938, caused great excitement and has been 
 referred to as the "zoological find of the 
 twentieth century." After millions of years,
  they continue to remain relatively unchanged. ';

return [
    [
        'film_id' => 1,
        'user_id' => 1,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 1,
        'user_id' => 2,
        'title' => 'bad film',
        'content' => $text,
        'rating' => 2
    ],
    [
        'film_id' => 1,
        'user_id' => 3,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 2,
        'user_id' => 4,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 2,
        'user_id' => 5,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 2,
        'user_id' => 6,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 3,
        'user_id' => 1,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 3,
        'user_id' => 2,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 3,
        'user_id' => 3,
        'title' => 'bad film',
        'content' => $text,
        'rating' => 2
    ],
    [
        'film_id' => 4,
        'user_id' => 1,
        'title' => 'good film',
        'content' => $text,
        'rating' => 4
    ],
    [
        'film_id' => 4,
        'user_id' => 2,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 4,
        'user_id' => 3,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 5,
        'user_id' => 6,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 5,
        'user_id' => 2,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ],
    [
        'film_id' => 5,
        'user_id' => 9,
        'title' => 'good film',
        'content' => $text,
        'rating' => 5
    ]
];
