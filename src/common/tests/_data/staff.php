<?php
return [
    [
        'name' => 'Robert',
        'surname' => 'Downey Jr.'
    ],
    [
        'name' => 'Mark',
        'surname' => 'Fergus'
    ],
    [
        'name' => 'Hawk',
        'surname' => 'Ostby'
    ],
    [
        'name' => 'Art',
        'surname' => 'Marcum'
    ],
    [
        'name' => 'Matt',
        'surname' => 'Holloway'
    ],
    [
        'name' => 'Stan',
        'surname' => 'Lee'
    ],
    [
        'name' => 'Don',
        'surname' => 'Heck'
    ],
    [
        'name' => 'Larry',
        'surname' => 'Lieber'
    ],
    [
        'name' => 'Jack',
        'surname' => 'Kirby'
    ]
];

