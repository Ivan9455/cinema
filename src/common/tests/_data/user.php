<?php

use common\models\User;

return [
    [
        'username' => 'user1',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ1',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user1@gmail.com',
        'status' => User::STATUS_DELETED,
        'created_at' => '1402312317',
        'updated_at' => '1402312317',
    ],
    [
        'username' => 'user2',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ2',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user2@gmail.com',
        'status' => User::STATUS_DELETED,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user3',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ3',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user3@gmail.com',
        'status' => User::STATUS_DELETED,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user4',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ4',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user4@gmail.com',
        'status' => User::STATUS_INACTIVE,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user5',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ5',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user5@gmail.com',
        'status' => User::STATUS_INACTIVE,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user6',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ6',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user6@gmail.com',
        'status' => User::STATUS_INACTIVE,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user7',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ7',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user7@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user8',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ8',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user8@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ],
    [
        'username' => 'user9',
        'auth_key' => 'iQOp5VjiCTFt7-c-_b0lQlk3c_9HmHZ9',
        'password_hash' => '$2y$13$Ohrce5trm3NgTqXhYcE7NeL0MNVDySwqqI1dX.KjHHM66PjFym06m',
        'email' => 'user9@gmail.com',
        'status' => User::STATUS_ACTIVE,
        'created_at' => '1402312317',
        'updated_at' => '1402312317'
    ]
];
