<?php

namespace common\widgets;

use yii\base\Widget;
use common\models\Review;

class LatestReview extends Widget
{
    public $numReviews = null;

    public function run()
    {
        if ($this->numReviews !== null){
            $latestReview = Review::getLatest($this->numReviews);
        } else {
            $latestReview = Review::getLatest(3);
        }
        
        return $this->render('review', [
            'latestReview' => $latestReview
        ]);
    }
}
