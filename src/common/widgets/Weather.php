<?php

namespace common\widgets;

use common\models\Config;
use yii\bootstrap\Widget;
use common\models\Weather as ModelWeather;
use Yii;

class Weather extends Widget
{

    public function run()
    {
        if (Yii::$app->cache->exists(ModelWeather::KEY_CITY)) {
            $result = Yii::$app->cache->get(ModelWeather::KEY_CITY);
        } elseif (Config::getValue(ModelWeather::KEY_CITY) !== null) {
            $config = unserialize(Config::getValue(ModelWeather::KEY_CITY));
            $weather = new ModelWeather();
            $result = $weather->getInfo($config['url']);
            $result['city'] = urldecode($config['city']);
        } else {
            $result = [
                'response' => false
            ];
        }

        if ($result['response'] === true) {
            setlocale(LC_ALL, 'ru_RU', 'ru_RU.UTF-8', 'ru', 'russian');
            $result['time'] = strftime("%A, %d %B,%H:%M", time());
        }

        return $this->render('weather', [
            'result' => $result
        ]);
    }
}
