<?php

use evgeniyrru\yii2slick\Slick;
use frontend\assets\SliderFilmAsset;

/**
 * @var $this yii\web\View
 * @var $images array
 */

SliderFilmAsset::register($this);
echo Slick::widget([
    'items' => $images,
    'clientOptions' => [
        'autoplay' => true,
        'variableWidth' => true
    ],
]);
