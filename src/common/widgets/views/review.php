<?php
/* @var $review common\models\Review */
/* @var array $latestReview common\models\Review */
?>

<div class="latest__reviews col-sm-9">
    <h3>Latest reviews :</h3>
    <?php foreach ($latestReview as $review):  ?>
        <?php if($review->rating > 3): ?>
            <div class="review alert-success alert">
        <?php else: ?>
             <div class="review alert-danger alert">
        <?php endif; ?>
            <div class="review__film">
                <a href="/film/<?= $review->film->id ?>" class="badge badge-pill badge-primary">
                    <?= $review->film->title ?>
                </a>
            </div>
            <div class="review__title"><?= $review->title ?></div>
            <div class="review__content"><?= $review->content ?></div>
            <hr>
            <div class="review__user"><?= $review->user->username ?></div>
            <div class="review__rating">
                <i class="far fa-star"></i>
                <?= $review->rating ?>
            </div>
            <?php if (Yii::$app->authManager->checkAccess(Yii::$app->user->identity->id, 'deleteReview')):  ?>
                <div class="review__delete">
                    <a href="/review/delete/<?= $review->id ?>" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this review?" data-method="post">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>
