<div class="weather_widget col-sm-3">
    <?php if ($result['response'] === true): ?>
        <h3>Weather :</h3>
        <ul class="list-group">
            <li class="list-group-item">
                Город : <?= $result['city']?>
            </li>
            <li class="list-group-item">
                Влажность : <?= $result['humidity'] ?>
            </li>
            <li class="list-group-item">
                Ветер : <?= $result['wind'] ?>
            </li>
            <li class="list-group-item">
                Давление : <?= $result['pressure'] ?>
            </li>
            <li class="list-group-item">
                Температура : <?= $result['temperature'] ?>
            </li>
            <li class="list-group-item">
                <?= $result['time'] ?>
            </li>
        </ul>
    <?php endif; ?>
</div>
