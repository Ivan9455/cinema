<?php

namespace common\widgets;

use common\models\Config;
use yii\base\Widget;
use common\models\Film;
use yii\helpers\Html;

class SliderFilm extends Widget
{
    public function run()
    {
        $config_slider = Config::getValue('slider');

        if (isset($config_slider)) {
            $films = Film::find()->where(['id' => unserialize($config_slider)])->all();
        } else {
            $films = Film::find()->orderBy('rating_kinopoisk DESC')->limit('3')->all();
        }

        return $this->render('slider_film', ['images' => $this->getImages($films)]);
    }

    protected function getImages($films)
    {
        $images = [];
        foreach ($films as $film) {
            $images[] = Html::a(Html::img('/uploads/' . $film->img), 'film/' . $film->id);
        }

        return $images;
    }
}
