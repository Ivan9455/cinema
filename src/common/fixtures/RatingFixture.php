<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class RatingFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Rating';
    public $dataFile = '@common/tests/_data/rating.php';
}
