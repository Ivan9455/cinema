<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

class AssignmentFixture extends ActiveFixture
{
    public $modelClass = 'yii\rbac\Assignment';
    public $dataFile = '@common/tests/_data/assignment.php';
    public $tableName = "auth_assignment";
}
