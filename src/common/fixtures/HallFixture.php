<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class HallFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Hall';
    public $dataFile = '@common/tests/_data/hall.php';
}
