<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class ReviewFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Review';
    public $dataFile = '@common/tests/_data/review.php';
}
