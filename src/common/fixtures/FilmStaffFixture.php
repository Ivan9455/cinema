<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class FilmStaffFixture extends ActiveFixture
{
    public $modelClass = 'common\models\FilmStaff';
    public $dataFile = '@common/tests/_data/film_staff.php';
}

