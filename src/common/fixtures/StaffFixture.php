<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class StaffFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Staff';
    public $dataFile = '@common/tests/_data/staff.php';
}
