<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class SeanceFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Seance';
    public $dataFile = '@common/tests/_data/seance.php';
}
