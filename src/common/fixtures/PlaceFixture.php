<?php
namespace common\fixtures;

use yii\test\ActiveFixture;

class PlaceFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Place';
    public $dataFile = '@common/tests/_data/place.php';
}
