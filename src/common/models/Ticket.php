<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $user_id
 * @property int $seance_id
 * @property int $place_id
 * @property string $status
 * @property int $price
 * @property string $token
 * @property string $reservation_code
 * @property string $ics_name
 *
 * @property Place $place
 * @property Seance $seance
 * @property User $user
 * @property array $places
 */
class Ticket extends ActiveRecord
{
    public const SEND_MAIL = 'sendMail';
    public const STATUS_RESERVATION = 1;
    public const STATUS_BUY = 2;
    public $places;

    public function init()
    {
        $this->on(self::SEND_MAIL, [$this, self::SEND_MAIL]);

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'default', 'value' => Yii::$app->user->identity->id],
            [['seance_id', 'place_id', 'status', 'price'], 'required'],
            [['seance_id', 'place_id', 'status'], 'integer'],
            [['price'], 'double'],
            [['token', 'reservation_code', 'ics_name'], 'string', 'max' => 255],
            [
                ['place_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Place::class,
                'targetAttribute' => ['place_id' => 'id']
            ],
            [
                ['seance_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Seance::class,
                'targetAttribute' => ['seance_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'seance_id' => 'Seance ID',
            'place_id' => 'Place ID',
            'status' => 'Status',
            'price' => 'Price',
            'token' => 'Token',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlace()
    {
        return $this->hasOne(Place::class, ['id' => 'place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeance()
    {
        return $this->hasOne(Seance::class, ['id' => 'seance_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return float
     */
    public function getFinalPrice() : float
    {
        return (float)Ticket::find()->where([
            'user_id' => Yii::$app->user->id,
            'seance_id' => $this->seance_id,
            'reservation_code' => $this->reservation_code,
        ])->sum('price');
    }

    /**
     * @param int $seance_id
     *
     * @return bool
     */
    public static function isExist(int $seance_id) : bool
    {
        return Ticket::find()->where([
            'user_id' => Yii::$app->user->id,
            'seance_id' => $seance_id,
        ])->exists();
    }

    /**
     * Event method
     * @throws \yii\base\Exception
     */
    public function sendMail()
    {
        Yii::$app->mailer->compose('template_mail', ['ticket' => $this])
            ->setFrom('cinema@cinema.com')
            ->setTo(Yii::$app->user->identity->email)
            ->setSubject($this->seance->film->title)
            ->attach(Yii::$app->calendar->template([
                'title' => $this->seance->film->title,
                'description' => '|' . $this->reservation_code . '|' . ' show this code!',
                'date_start' => $this->seance->start,
                'date_end' => $this->seance->end
            ])->create($this->ics_name))
            ->send();
    }
}
