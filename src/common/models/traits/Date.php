<?php

namespace common\models\traits;

use DateTime;
use DateInterval;

trait Date
{
    /**
     * @param string $date
     *
     * @return DateTime
     * @throws \Exception
     */
    private function setDate(string $date) : DateTime
    {
        return new DateTime($date);
    }

    /**
     * @param string $time
     * @param string $format
     * @param string $date
     *
     * @return string
     * @throws \Exception
     */
    public function getDate(string $time, string $format = 'Y-m-d', string $date = 'now') : string
    {
        return $this->setDate($date)->add(DateInterval::createFromDateString($time))->format($format);
    }
}
