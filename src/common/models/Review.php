<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "review".
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $content
 * @property int $film_id
 *
 * @property Film $film
 * @property User $user
 */
class Review extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'default', 'value' => Yii::$app->user->identity->id],
            [['title', 'content', 'film_id'], 'required'],
            [['film_id', 'rating'], 'integer'],
            [['title', 'content'], 'string'],
            [['rating'], 'validateRating'],
            [
                ['film_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Film::class,
                'targetAttribute' => ['film_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    public function validateRating($attribute)
    {
        if ($this->rating < 1 || $this->rating > 5) {
            $this->addError($attribute, 'Sorry, incorrect rating');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'content' => 'Content',
            'film_id' => 'Film ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Film::class, ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @param int $num
     *
     * @return Review[]
     */
    public static function getLatest(int $num) : array
    {
        return Review::find()->orderBy(['id' => SORT_DESC])->limit($num)->all();
    }
}
