<?php

namespace common\models;

use common\models\traits\Date;

/**
 * This is the model class for table "film".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $time
 * @property string $country
 * @property string $datetime
 * @property string $img
 * @property double rating_kinopoisk
 * @property double rating_imdb
 * @property double rating_rotten_tomatoes
 */
class Film extends \yii\db\ActiveRecord
{
    use Date;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'film';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'time', 'country', 'release_date'], 'required'],
            [['title', 'description'], 'string'],
            [['time'], 'safe'],
            [['rating_kinopoisk', 'rating_imdb', 'rating_rotten_tomatoes'], 'double'],
            [['country'], 'string', 'max' => 255],
            [['img'], 'file', 'extensions' => 'png, jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'time' => 'Time',
            'country' => 'Country',
            'release_date' => 'Release Date',
            'img' => 'Img',
            'rating_kinopoisk' => 'Rating Kinopoisk',
            'rating_imdb' => 'Rating IMDB',
            'rating_rotten_tomatoes' => 'Rating Rotten Tomatoes'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilmStaff()
    {
        return $this->hasMany(FilmStaff::class, ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatings()
    {
        return $this->hasMany(Rating::class, ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Review::class, ['film_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeances()
    {
        return $this->hasMany(Seance::class, ['film_id' => 'id']);
    }

    /**
     * @param string|null $date
     *
     * @return array
     */
    public function getCurrentSeances(string $date = null) : array
    {
        if (empty($date)) {
            $date = $this->getDate('today');
        }

        return $this->getSeances()->andFilterWhere(['like', 'seance.start', $date])->all();
    }
}
