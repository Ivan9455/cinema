<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "config".
 *
 * @property int $id
 * @property string $key
 * @property string $value
 */
class Config extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName() : string
    {
        return 'config';
    }

    /**
     * @param string $key
     *
     * @return $this
     */
    public function setKey(string $key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key', 'value'], 'string'],
            [['key'], 'string', 'max' => 255],
        ];
    }

    /**
     * @param string $key
     *
     * @return string
     */
    public static function getValue(string $key)
    {
        return self::findOne(['key' => $key])->value;
    }
}
