<?php

namespace common\models\film;

/**
 * Class Info
 * @package common\models\film
 * @property string $img
 * @property string $title
 * @property string $year
 * @property float $rating
 */
class Info
{
    public $img;
    public $title;
    public $year;
    public $rating;

    public function __construct(string $img, string $title, string $year, float $rating)
    {
        $this->img = $img;
        $this->title = $title;
        $this->year = $year;
        $this->rating = $rating;
    }
}
