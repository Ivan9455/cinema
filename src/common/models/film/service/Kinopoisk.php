<?php

namespace common\models\film\service;

use SimpleXMLElement;
use yii\web\BadRequestHttpException;
use common\models\film\Service;
use common\models\film\Info;

class Kinopoisk extends Service
{
    protected const COUNT_FILM = 6;

    /**
     * @param string $title
     *
     * @return array
     */
    public function find(string $title) : array
    {
        $res = [
            'response' => false
        ];

        $result = file_get_contents($this->getUrl($title));

        if ($result === false) {
            return $res;
        }

        preg_match_all('#<p class="pic">(.*?)<\/p>#i', $result, $data);
        preg_match_all('#<p class="name">(.*?)<\/p>#i', $result, $info);
        for ($i = 0; $i < self::COUNT_FILM; $i++) {
            if (!isset($data[1][$i], $info[1][$i])) {
                return $res;
            }
            preg_match_all('#data-id="(.*?)"#i', $data[1][$i], $id);
            preg_match_all('#<img class=\'flap_img\' src="(.*?)"#', $data[1][$i], $img);
            preg_match_all('#>(.*?)<\/a>#', $info[1][$i], $title);
            preg_match_all('#class="year">(.*?)<#', $info[1][$i], $year);

            if (!isset($id[1][0])) {
                return $res;
            }
            $id = $id[1][0];
            $img = $img[1][0] ?? '';
            $title = $title[1][0] ?? '';
            $year = $year[1][0] ?? '';

            $kp_rating = $this->getRating($id) ?? 0;

            if ($kp_rating === false) {
                return $res;
            }

            $res['films'][] = new Info($img, $title, $year, $kp_rating);
        }
        $res['response'] = true;

        return $res;
    }

    /**
     *(@inheritdoc)
     */
    protected function getUrl(string $title) : string
    {
        return sprintf('https://www.kinopoisk.ru/index.php?kp_query=%s', urlencode($title));
    }

    /**
     * @param string $id
     *
     * @return float|false
     */
    protected function getRating(string $id)
    {
        $xml = file_get_contents(sprintf('https://rating.kinopoisk.ru/%s.xml', $id));

        if ($xml === false) {
            return false;
        }

        $rating = new SimpleXMLElement($xml);

        if (!isset($rating->kp_rating)) {
            return false;
        }

        return floatval($rating->kp_rating);
    }
}
