<?php

namespace common\models\film\service;

use common\models\film\Info;
use common\models\film\Service;

class RottenTomatoes extends Service
{
    /**
     * @param string $title
     *
     * @return array
     */
    public function find(string $title) : array
    {
        $res = [
            'response' => false
        ];

        $result = file_get_contents($this->getUrl($title));

        if ($result === false) {
            return $res;
        }

        preg_match_all("#'$title', (.*?)\);#", $result, $data);
        $json = json_decode($data[1][0], true);

        if (!isset($data[1][0], $json['movies'])) {
            return $res;
        }

        $films = $json['movies'];
        foreach ($films as $film) {
            $res['films'][] = new Info($film['image'], $film['name'], $film['year'],
                floatval($film['meterScore'] / 10));
        }
        $res['response'] = true;

        return $res;
    }

    /**
     *(@inheritdoc)
     */
    protected function getUrl(string $title) : string
    {
        return sprintf('https://www.rottentomatoes.com/search/?search=%s', urlencode($title));
    }
}
