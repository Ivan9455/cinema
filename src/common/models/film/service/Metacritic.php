<?php

namespace common\models\film\service;

use common\models\film\Info;
use yii\web\BadRequestHttpException;
use common\models\film\Service;

class Metacritic extends Service
{
    /**
     * @param string $title
     *
     * @return array
     */
    public function find(string $title) : array
    {
        $res = [
            'response' => false
        ];

        $result = file_get_contents($this->getUrl($title));

        if ($result === false) {
            return $res;
        }
        $result = preg_replace('|[\s]+|s', ' ', $result);
        preg_match_all('#<div class="result_wrap">(.*?)<\/div> <\/li>#', $result, $data);
        if (!isset($data[1])) {
            return $res;
        }
        for ($i = 0; $i < count($data[1]); $i++) {
            if (!isset($data[1][$i])) {
                return $res;
            }

            preg_match_all('#<img src="(.*?)"#', $data[1][$i], $img);
            preg_match_all('#<span class="(.*?)">(.*?)<\/span>#', $data[1][$i], $rating);
            preg_match_all('#<a href="(.*?)">(.*?)<\/a>#', $data[1][$i], $title);
            preg_match_all('#<p>(.*?),(.*?)<\/p>#', $data[1][$i], $year);

            $img = $img[1][0] ?? '';
            $title = trim($title[2][0]) ?? '';
            $year = trim($year[2][0]) ?? '';
            $rating = floatval($rating[2][0] ?? 0) / 10;

            $res['films'][] = new Info($img, $title, $year, $rating);
        }
        $res['response'] = true;

        return $res;
    }

    /**
     *(@inheritdoc)
     */
    protected function getUrl(string $title) : string
    {
        return sprintf('https://www.metacritic.com/search/movie/%s/results', urlencode($title));
    }
}
