<?php

namespace common\models\film\service;

use common\models\film\Info;
use common\models\film\Service;

class IMDB extends Service
{
    /**
     * @param string $title
     *
     * @return array
     */
    public function find(string $title) : array
    {
        $res = [
            'response' => false
        ];

        $api = file_get_contents($this->getUrl($title));
        if ($api === false) {
            return $res;
        }
        $api = json_decode($api, true);
        if (!isset($api['Search'])) {
            return $res;
        }
        foreach ($api['Search'] as $film) {
            if (!isset($film['imdbID'])) {
                return $res;
            }

            $rating = $this->getRating($film['imdbID']);

            if ($rating === false) {
                return $res;
            }

            $res['films'][] = new Info($film['Poster'] ?? '', $film['Title'] ?? '', $film['Year'] ?? '', $rating ?? 0);
        }
        $res['response'] = true;

        return $res;
    }

    /**
     *(@inheritdoc)
     */
    protected function getUrl(string $title) : string
    {
        return sprintf('http://www.omdbapi.com/?s=%s&apikey=%s', urlencode($title), $this->getApiKey());
    }

    /**
     * @return string
     */
    protected function getApiKey() : string
    {
        return '72342a20';
    }

    /**
     * @param string $id
     *
     * @return false|float
     */
    protected function getRating(string $id)
    {
        $rating = file_get_contents($this->getUrlApi($id));
        if ($rating === false) {
            return false;
        }

        $rating = json_decode($rating, true);
        if (!isset($rating['imdbRating'])) {
            return false;
        }

        return $rating['imdbRating'];
    }

    /**
     * @param string $id
     *
     * @return string
     */
    protected function getUrlApi(string $id) : string
    {
        return sprintf('http://www.omdbapi.com/?i=%s&apikey=%s', $id, $this->getApiKey());
    }
}
