<?php

namespace common\models\film\service;

use common\models\film\Service;
use Exception;

class Factory
{
    public const IMDB = 0;
    public const ROTTEN_TOMATOES = 1;
    public const METACRITIC = 2;
    public const KINOPOISK = 3;

    /**
     * @param int $service
     *
     * @return Service
     * @throws Exception
     */
    public static function create(int $service) : Service
    {
        switch ($service) {
            case self::IMDB:
                return new IMDB();
            case self::ROTTEN_TOMATOES:
                return new RottenTomatoes();
            case self::METACRITIC:
                return new Metacritic();
            case self::KINOPOISK:
                return new Kinopoisk();
            default:
                throw new Exception('service not found');
        }
    }
}
