<?php

namespace common\models\film;

abstract class Service
{
    /**
     * @param string $title
     *
     * @return array
     */
    abstract public function find(string $title) : array;

    /**
     * @param string $title
     *
     * @return string
     */
    abstract protected function getUrl(string $title) : string;
}
