<?php

namespace common\models;

use Yii;
use DateTime;

/**
 * This is the model class for table "seance".
 *
 * @property int $id
 * @property int $price
 * @property int $hall_id
 * @property int $film_id
 * @property string $start
 * @property string $end
 * @property Ticket[] $tickets
 * @property Film $film
 * @property Hall $hall
 */
class Seance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'seance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'hall_id', 'film_id'], 'required'],
            [['price', 'hall_id', 'film_id'], 'integer'],
            [['start', 'end'], 'safe'],
            [
                ['film_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Film::class,
                'targetAttribute' => ['film_id' => 'id']
            ],
            [
                ['hall_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Hall::class,
                'targetAttribute' => ['hall_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'hall_id' => 'Hall ID',
            'film_id' => 'Film ID',
            'start' => 'Start',
            'end' => 'End',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFilm()
    {
        return $this->hasOne(Film::class, ['id' => 'film_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHall()
    {
        return $this->hasOne(Hall::class, ['id' => 'hall_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::class, ['seance_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFreePlaces()
    {
        return Place::find()->where(['hall_id' => $this->hall_id])->andWhere(['is', 'ticket.place_id', null])
            ->join('LEFT OUTER JOIN', 'ticket', 'ticket.place_id = place.id ');
    }

    /**
     * @return array
     */
    public function getPrice() : array
    {
        $res = [];
        foreach ($this->getFreePlaces()->all() as $place) {
            $res[$place->id] =
                ' row : ' . $place->row .
                ' position : ' . $place->position .
                ' price : ' . $place->rate * $this->price;
        }

        return $res;
    }

    /**
     * @param string|null $date
     *
     * @return array
     */
    public static function getCurrentFilms(string $date = null) : array
    {
        if (empty($date)) {
            $date = (new DateTime('now'))->format('Y-m-d');
        }

        return Film::find()
            ->innerJoin('seance', 'seance.film_id = film.id')
            ->andFilterWhere(['like', 'seance.start', $date])
            ->all();
    }
}
