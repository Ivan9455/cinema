<?php

namespace common\models;

class Weather
{
    public const KEY_CITY = 'weather_city';
    public const CACHE_DURATION = 600;

    /**
     * @param string $url_city
     *
     * @return array
     */
    public function getInfo(string $url_city) : array
    {
        $result = [
            'response' => false
        ];
        $file = $this->getPage($this->getUrlInfo($url_city));

        if (isset($file['error']) || (isset($file['data']) && $file['data'] === false)) {
            $result['error'] = $file['error'];

            return $result;
        }

        preg_match_all('#<div class="nowinfo__value">(.*?)<\/div>#', $file['data'], $info);
        if (!isset($info[1], $info[1][0], $info[1][3], $info[1][4], $info[1][5])) {
            $result['error'] = 'An error occurred while reading data';

            return $result;
        }

        return [
            'response' => true,
            'temperature' => $info[1][5],
            'pressure' => $info[1][0],
            'humidity' => $info[1][3],
            'wind' => $info[1][4]
        ];
    }

    /**
     * @param string $city_name
     *
     * @return array
     */
    public function getSearchCities(string $city_name) : array
    {
        $result = [
            'response' => false
        ];
        $file = $this->getPage($this->getUrlSearch($city_name));

        if (isset($file['error']) || (isset($file['data']) && $file['data'] === false)) {
            $result['error'] = $file['error'];

            return $result;
        }

        preg_match_all('#<div class="catalog_item">(.*?)<\/div>#', $file['data'], $data);

        if (!isset($data[0])) {
            $result['error'] = 'an error occurred while reading the regular expression data';

            return $result;
        }

        $cities = [];
        for ($i = 0; $i < count($data[0]); $i++) {
            preg_match_all('#<a href="(.*?)"#', $data[0][$i], $url);
            preg_match_all('#<\/i>(.*?)<\/a>#', $data[0][$i], $city);
            preg_match_all('#gray">(.*?)<\/a>#', $data[0][$i], $area);

            if (!isset($url[1][0], $city[1][0], $area[1])) {
                $result['error'] = 'An error occurred while reading data';

                return $result;
            }
            $cities[] = [
                'url' => $url[1][0],
                'city' => $city[1][0],
                'area' => $city[1][0] . ' , ' . implode(' , ', $area[1])
            ];
        }

        return [
            'response' => true,
            'cities' => $cities
        ];
    }

    /**
     * @param string $url_city
     *
     * @return string
     */
    protected function getUrlInfo(string $url_city) : string
    {
        return sprintf('https://www.gismeteo.ru%snow/', $url_city);
    }

    /**
     * @param string $city_name
     *
     * @return string
     */
    protected function getUrlSearch(string $city_name) : string
    {
        return sprintf('https://www.gismeteo.ru/search/%s/', urlencode($city_name));
    }

    /**
     * @param string $url
     *
     * @return array
     */
    protected function getPage(string $url) : array
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        if ($data === false) {
            return [
                'error' => curl_error($ch)
            ];
        }

        return [
            'data' => $data
        ];
    }
}
