<?php

namespace common\models;

use common\models\traits\Date;
use Yii;
use yii\base\Exception;

class Calendar
{
    use Date;

    private $template;
    private $data = [
        'title',
        'description',
        'date_start',
        'date_end'
    ];

    /**
     * @param array $data
     *
     * @return bool
     */
    private function setData(array $data) : bool
    {
        if (empty($data)) {
            return false;
        }

        foreach ($this->data as $value) {
            if (!isset($data[$value])) {
                return false;
            }
        }

        $this->data = $data;

        return true;
    }

    /**
     * @param string $file_name
     *
     * @return string
     */
    public function create(string $file_name) : string
    {
        $file_name = Yii::getAlias('@frontend') . '/web/events/' . $file_name . '.ics';
        $file = fopen($file_name, 'x');
        fwrite($file, $this->template);
        fclose($file);

        return $file_name;
    }

    /**
     * @param array $data
     *
     * @return Calendar
     * @throws Exception
     */
    public function template(array $data) : Calendar
    {
        if (!$this->setData($data)) {
            throw new Exception("not found data");
        }

        $this->template = "BEGIN:VCALENDAR\n";
        $this->template .= "PRODID:Calendar\n";
        $this->template .= "VERSION:2.0\n";
        $this->template .= "BEGIN:VEVENT\n";
        $this->template .= "UID:0@default\n";
        $this->template .= "CLASS:PUBLIC\n";
        $this->template .= "DESCRIPTION:{$this->data['description']}\n";
        $this->template .= "DTSTAMP;VALUE=DATE-TIME:{$this->timeFormat($this->getDate('now'))}\n";
        $this->template .= "DTSTART;VALUE=DATE-TIME:{$this->timeFormat($this->data['date_start'])}\n";
        $this->template .= "DTEND;VALUE=DATE-TIME:{$this->timeFormat($this->data['date_end'])}\n";
        $this->template .= "LOCATION:New York\n";
        $this->template .= "SUMMARY;LANGUAGE=en-us:{$this->data['title']}\n";
        $this->template .= "TRANSP:TRANSPARENT\n";
        $this->template .= "END:VEVENT\n";
        $this->template .= "END:VCALENDAR\n";

        return $this;
    }

    /**
     * @param string $time
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    private function timeFormat(string $time) : string
    {
        return Yii::$app->formatter->asDate($time, 'php:Ymd') . 'T' .
            Yii::$app->formatter->asDate($time, 'php:His');
    }
}
