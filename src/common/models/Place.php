<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "place".
 *
 * @property int $id
 * @property int $hall_id
 * @property int $row
 * @property int $position
 * @property double $rate
 *
 * @property Hall $hall
 * @property Ticket[] $tickets
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'place';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hall_id', 'row', 'position', 'rate'], 'required'],
            [['hall_id', 'row', 'position'], 'integer'],
            [['rate'], 'number'],
            [
                ['hall_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Hall::class,
                'targetAttribute' => ['hall_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hall_id' => 'Hall ID',
            'row' => 'Row',
            'position' => 'Position',
            'rate' => 'Rate',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHall()
    {
        return $this->hasOne(Hall::class, ['id' => 'hall_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::class, ['place_id' => 'id']);
    }
}
