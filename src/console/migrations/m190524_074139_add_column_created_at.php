<?php

use yii\db\Migration;

/**
 * Class m190524_074139_add_column_created_at
 */
class m190524_074139_add_column_created_at extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('review', 'created_at', $this->timestamp()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('review', 'created_at');
    }
}
