<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rating}}`.
 */
class m190502_082245_create_rating_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rating}}', [
            'id' => $this->primaryKey(),
            'film_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'value' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-rating-user_id',
            'rating',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-rating-film_id',
            'rating',
            'film_id',
            'film',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-rating-user_id',
            'rating'
        );

        $this->dropForeignKey(
            'fk-rating-film_id',
            'rating'
        );

        $this->dropTable('{{%rating}}');
    }
}
