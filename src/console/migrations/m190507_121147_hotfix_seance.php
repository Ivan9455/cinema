<?php

use yii\db\Migration;

/**
 * Class m190507_121147_hotfix_seance
 */
class m190507_121147_hotfix_seance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('seance', 'start', 'datetime');
        $this->alterColumn('seance', 'end', 'datetime');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('seance', 'start', 'time');
        $this->alterColumn('seance', 'end', 'time');
    }
}
