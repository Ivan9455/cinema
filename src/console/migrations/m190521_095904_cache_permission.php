<?php

use yii\db\Migration;

/**
 * Class m190521_095904_cache_permission
 */
class m190521_095904_cache_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $setCache = $auth->createPermission('setCache');
        $setCache->description = 'set cache';
        $auth->add($setCache);

        $deleteCache = $auth->createPermission('deleteCache');
        $deleteCache->description = 'delete cache';
        $auth->add($deleteCache);

        $getCache = $auth->createPermission('getCache');
        $getCache->description = 'get cache';
        $auth->add($getCache);

        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $setCache);
        $auth->addChild($admin, $deleteCache);
        $auth->addChild($admin, $getCache);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $auth->removeChild($admin, $auth->getPermission('setCache'));
        $auth->removeChild($admin, $auth->getPermission('deleteCache'));
        $auth->removeChild($admin, $auth->getPermission('getCache'));
    }
}
