<?php

use yii\db\Migration;

/**
 * Class m190614_115111_fix_charset
 */
class m190614_115111_fix_charset extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->updateCharsetDb('utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->updateCharsetDb('utf8_unicode_ci');
    }

    public function updateCharsetDb(string $charset)
    {
        $db = $this->db->createCommand("select database()")->queryOne()['database()'];
        Yii::$app->db->createCommand('ALTER DATABASE ' . $db . ' COLLATE ' . $charset)->execute();

        $tables = Yii::$app->db->schema->tableNames;

        $tables = array_flip($tables);
        unset($tables['auth_assignment'], $tables['auth_item'], $tables['auth_item_child'], $tables['auth_rule']);
        $tables = array_flip($tables);

        foreach ($tables as $table) {
            $this->db->createCommand("ALTER TABLE  {$db}.{$table} CONVERT TO CHARACTER SET utf8 COLLATE {$charset}")->execute();
        }
    }
}
