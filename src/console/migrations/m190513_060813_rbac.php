<?php

use yii\db\Migration;

/**
 * Class m190513_060813_rbac
 */
class m190513_060813_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        //Permission User
        $viewReview = $auth->createPermission('viewReview');
        $viewReview->description = 'view review';
        $auth->add($viewReview);

        $editReview = $auth->createPermission('editReview');
        $editReview->description = 'edit review';
        $auth->add($editReview);

        $createReview = $auth->createPermission('createReview');
        $createReview->description = 'create review';
        $auth->add($createReview);

        $viewRating = $auth->createPermission('viewRating');
        $viewRating->description = 'view rating';
        $auth->add($viewRating);

        $createRating = $auth->createPermission('createRating');
        $createRating->description = 'create rating';
        $auth->add($createRating);

        $payTicket = $auth->createPermission('payTicket');
        $payTicket->description = 'pay ticket';
        $auth->add($payTicket);

        $reservationTicket = $auth->createPermission('reservationTicket');
        $reservationTicket->description = 'reservation ticket';
        $auth->add($reservationTicket);

        //Permission Moderator
        $viewAdminPage = $auth->createPermission('viewAdminPage');
        $viewAdminPage->description = 'view admin page';
        $auth->add($viewAdminPage);

        $editPlace = $auth->createPermission('editPlace');
        $editPlace->description = 'edit page';
        $auth->add($editPlace);

        $createPlace = $auth->createPermission('createPlace');
        $createPlace->description = 'create page';
        $auth->add($createPlace);

        $editSeance = $auth->createPermission('editSeance');
        $editSeance->description = 'edit seance';
        $auth->add($editSeance);

        $createSeance = $auth->createPermission('createSeance');
        $createSeance->description = 'create seance';
        $auth->add($createSeance);

        $viewModeratorFilm = $auth->createPermission('viewModeratorFilm');
        $viewModeratorFilm->description = 'view moderator film';
        $auth->add($viewModeratorFilm);

        $editFilm = $auth->createPermission('editFilm');
        $editFilm->description = 'edit film';
        $auth->add($editFilm);

        $createFilm = $auth->createPermission('createFilm');
        $createFilm->description = 'create film';
        $auth->add($createFilm);

        $editStaff = $auth->createPermission('editStaff');
        $editStaff->description = 'edit staff';
        $auth->add($editStaff);

        $createStaff = $auth->createPermission('createStaff');
        $createStaff->description = 'create staff';
        $auth->add($createStaff);

        //Permission Admin
        $deleteReservationTicket = $auth->createPermission('deleteReservationTicket');
        $deleteReservationTicket->description = 'delete reservation ticket';
        $auth->add($deleteReservationTicket);

        $deletePlace = $auth->createPermission('deletePlace');
        $deletePlace->description = 'delete place';
        $auth->add($deletePlace);

        $deleteSeance = $auth->createPermission('deleteSeance');
        $deleteSeance->description = 'delete seance';
        $auth->add($deleteSeance);

        $deleteFilm = $auth->createPermission('deleteFilm');
        $deleteFilm->description = 'delete film';
        $auth->add($deleteFilm);

        $deleteStaff = $auth->createPermission('deleteStaff');
        $deleteStaff->description = 'delete staff';
        $auth->add($deleteStaff);

        $deleteReview = $auth->createPermission('deleteReview');
        $deleteReview->description = 'delete review';
        $auth->add($deleteReview);

        $user = $auth->createRole('user');
        $auth->add($user);
        $auth->addChild($user, $viewReview);
        $auth->addChild($user, $editReview);
        $auth->addChild($user, $createReview);
        $auth->addChild($user, $viewRating);
        $auth->addChild($user, $createRating);
        $auth->addChild($user, $payTicket);
        $auth->addChild($user, $reservationTicket);

        $moderator = $auth->createRole('moderator');
        $auth->add($moderator);
        $auth->addChild($moderator, $user);
        $auth->addChild($moderator, $viewAdminPage);
        $auth->addChild($moderator, $editPlace);
        $auth->addChild($moderator, $createPlace);
        $auth->addChild($moderator, $editSeance);
        $auth->addChild($moderator, $createSeance);
        $auth->addChild($moderator, $viewModeratorFilm);
        $auth->addChild($moderator, $createFilm);
        $auth->addChild($moderator, $editFilm);
        $auth->addChild($moderator, $createStaff);
        $auth->addChild($moderator, $editStaff);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $moderator);
        $auth->addChild($admin, $deletePlace);
        $auth->addChild($admin, $deleteSeance);
        $auth->addChild($admin, $deleteFilm);
        $auth->addChild($admin, $deleteStaff);
        $auth->addChild($admin, $deleteReview);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
