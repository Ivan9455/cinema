<?php

use yii\db\Migration;

/**
 * Class m190614_122458_user_status
 */
class m190614_122458_user_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user', 'status', $this->smallInteger()->notNull()->defaultValue(9));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('user', 'status', $this->smallInteger()->notNull()->defaultValue(10));
    }
}
