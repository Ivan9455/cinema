<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%film}}`.
 */
class m190502_070420_create_film_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%film}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'time' => $this->time()->notNull(),
            'country' => $this->string()->notNull(),
            'year' => $this->integer()->notNull(),
            'img' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%film}}');
    }
}
