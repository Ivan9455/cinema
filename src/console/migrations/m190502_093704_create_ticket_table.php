<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ticket}}`.
 */
class m190502_093704_create_ticket_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%ticket}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'seance_id' => $this->integer()->notNull(),
            'place_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'price' => $this->integer()->notNull(),
            'token' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-ticket-user_id',
            'ticket',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-ticket-place_id',
            'ticket',
            'place_id',
            'place',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-ticket-seance_id',
            'ticket',
            'seance_id',
            'seance',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-ticket-user_id',
            'ticket'
        );

        $this->dropForeignKey(
            'fk-ticket-place_id',
            'ticket'
        );

        $this->dropForeignKey(
            'fk-ticket-seance_id',
            'ticket'
        );

        $this->dropTable('{{%ticket}}');
    }
}
