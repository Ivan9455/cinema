<?php

use yii\db\Migration;

/**
 * Class m190516_092330_add_columns_ratings_film
 */
class m190516_092330_add_columns_ratings_film extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('film', 'rating_kinopoisk', $this->float());
        $this->addColumn('film', 'rating_imdb', $this->float());
        $this->addColumn('film', 'rating_rotten_tomatoes', $this->float());
        $this->renameColumn('film', 'year', 'release_date');
        $this->alterColumn('film', 'release_date', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('film', 'rating_kinopoisk');
        $this->dropColumn('film', 'rating_imdb');
        $this->dropColumn('film', 'rating_rotten_tomatoes');
        $this->renameColumn('film', 'release_date', 'year');
        $this->alterColumn('film', 'year', $this->integer());
    }
}
