<?php

use yii\db\Migration;
use common\models\Config;

/**
 * Class m190617_071215_add_config
 */
class m190617_071215_add_config extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        (new Config())->setKey('slider')->setValue(serialize([1, 3, 5]))->save();
        (new Config())->setKey('ticket/restriction')->setValue(5)->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Config::find()->where(['key' => 'slider'])->one()->delete();
        Config::find()->where(['key' => 'ticket/restriction'])->one()->delete();
    }
}
