<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%film_staff}}`.
 */
class m190502_095050_create_film_staff_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%film_staff}}', [
            'id' => $this->primaryKey(),
            'film_id' => $this->integer()->notNull(),
            'staff_id' => $this->integer()->notNull(),
            'role' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-film_staff-film_id',
            'film_staff',
            'film_id',
            'film',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-film_staff-staff_id',
            'film_staff',
            'staff_id',
            'staff',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-film_staff-film_id',
            'film_staff'
        );

        $this->dropForeignKey(
            'fk-film_staff-staff_id',
            'film_staff'
        );

        $this->dropTable('{{%film_staff}}');
    }
}
