<?php

use yii\db\Migration;

/**
 * Class m190603_112639_change_type_price_ticket
 */
class m190603_112639_change_type_price_ticket extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('ticket', 'price', $this->float()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('ticket', 'price', $this->integer()->notNull());
    }
}
