<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seance}}`.
 */
class m190502_081556_create_seance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seance}}', [
            'id' => $this->primaryKey(),
            'price' => $this->float()->notNull(),
            'hall_id' => $this->integer()->notNull(),
            'film_id' => $this->integer()->notNull(),
            'start' => $this->time()->notNull(),
            'end' => $this->time()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-seance-hall_id',
            'seance',
            'hall_id',
            'hall',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-seance-film_id',
            'seance',
            'film_id',
            'film',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-seance-hall_id',
            'seance'
        );

        $this->dropForeignKey(
            'fk-seance-film_id',
            'seance'
        );

        $this->dropTable('{{%seance}}');
    }
}
