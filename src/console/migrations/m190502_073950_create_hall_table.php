<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%hall}}`.
 */
class m190502_073950_create_hall_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%hall}}', [
            'id' => $this->primaryKey(),
            'number' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%hall}}');
    }
}
