<?php

use yii\db\Migration;

/**
 * Class m190515_132050_add_permission
 */
class m190515_132050_add_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        //Ticket permission
        $createTicket = $auth->createPermission('createTicket');
        $createTicket->description = 'create Ticket';
        $auth->add($createTicket);

        $updateTicket = $auth->createPermission('updateTicket');
        $updateTicket->description = 'update Ticket';
        $auth->add($updateTicket);

        $viewTicket = $auth->createPermission('viewTicket');
        $viewTicket->description = 'view ticket';
        $auth->add($viewTicket);

        $deleteTicket = $auth->createPermission('deleteTicket');
        $deleteTicket->description = 'delete Ticket';
        $auth->add($deleteTicket);

        //Staff permission
        $viewStaff = $auth->createPermission('viewStaff');
        $viewStaff->description = 'view Staff';
        $auth->add($viewStaff);

        //Seance
        $viewSeance = $auth->createPermission('viewSeance');
        $viewSeance->description = 'view Seance';
        $auth->add($viewSeance);

        //FilmStaff
        $viewFilmStaff = $auth->createPermission('viewFilmStaff');
        $viewFilmStaff->description = 'view FilmStaff';
        $auth->add($viewFilmStaff);

        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $createTicket);
        $auth->addChild($admin, $updateTicket);
        $auth->addChild($admin, $viewTicket);
        $auth->addChild($admin, $deleteTicket);
        $auth->addChild($admin, $viewStaff);
        $auth->addChild($admin, $viewSeance);
        $auth->addChild($admin, $viewFilmStaff);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $auth->removeChild($admin, $auth->getPermission('createTicket'));
        $auth->removeChild($admin, $auth->getPermission('updateTicket'));
        $auth->removeChild($admin, $auth->getPermission('viewTicket'));
        $auth->removeChild($admin, $auth->getPermission('deleteTicket'));
        $auth->removeChild($admin, $auth->getPermission('viewStaff'));
        $auth->removeChild($admin, $auth->getPermission('viewSeance'));
        $auth->removeChild($admin, $auth->getPermission('viewFilmStaff'));
    }
}
