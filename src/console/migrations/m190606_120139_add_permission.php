<?php

use yii\db\Migration;

/**
 * Class m190606_120139_add_permission
 */
class m190606_120139_add_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $viewConfig = $auth->createPermission('viewConfig');
        $viewConfig->description = 'view config';
        $auth->add($viewConfig);

        $viewConfigWeatherCity = $auth->createPermission('viewConfigWeatherCity');
        $viewConfigWeatherCity->description = 'view config weather city';
        $auth->add($viewConfigWeatherCity);

        $saveConfigWeatherCity = $auth->createPermission('saveConfigWeatherCity');
        $saveConfigWeatherCity->description = 'save config weather city';
        $auth->add($saveConfigWeatherCity);

        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $viewConfig);
        $auth->addChild($admin, $viewConfigWeatherCity);
        $auth->addChild($admin, $saveConfigWeatherCity);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $admin = $auth->getRole('admin');

        $auth->remove($auth->getPermission('viewConfig'));
        $auth->remove($auth->getPermission('viewConfigWeatherCity'));
        $auth->remove($auth->getPermission('saveConfigWeatherCity'));

        $auth->removeChild($admin, $auth->getPermission('viewConfig'));
        $auth->removeChild($admin, $auth->getPermission('viewConfigWeatherCity'));
        $auth->removeChild($admin, $auth->getPermission('saveConfigWeatherCity'));
    }
}
