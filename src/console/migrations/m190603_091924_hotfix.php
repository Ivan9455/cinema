<?php

use yii\db\Migration;

/**
 * Class m190603_091924_hotfix
 */
class m190603_091924_hotfix extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('seance', 'start', $this->dateTime()->notNull());
        $this->alterColumn('seance', 'end', $this->dateTime()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('seance', 'start', 'datetime');
        $this->alterColumn('seance', 'end', 'datetime');
    }
}
