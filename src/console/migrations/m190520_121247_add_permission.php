<?php

use yii\db\Migration;

/**
 * Class m190520_121247_add_permission
 */
class m190520_121247_add_permission extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        $deleteRating = $auth->createPermission('deleteRating');
        $deleteRating->description = 'delete rating';
        $auth->add($deleteRating);

        $updateRating = $auth->createPermission('updateRating');
        $updateRating->description = 'update rating';
        $auth->add($updateRating);

        $viewPlace = $auth->createPermission('viewPlace');
        $viewPlace->description = 'view Place';
        $auth->add($viewPlace);

        $createHall = $auth->createPermission('createHall');
        $createHall->description = 'create hall';
        $auth->add($createHall);

        $deleteHall = $auth->createPermission('deleteHall');
        $deleteHall->description = 'delete hall';
        $auth->add($deleteHall);

        $updateHall = $auth->createPermission('updateHall');
        $updateHall->description = 'update hall';
        $auth->add($updateHall);

        $viewHall = $auth->createPermission('viewHall');
        $viewHall->description = 'view hall';
        $auth->add($viewHall);

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'create user';
        $auth->add($createUser);

        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'update user';
        $auth->add($updateUser);

        $viewUser = $auth->createPermission('viewUser');
        $viewUser->description = 'view user';
        $auth->add($viewUser);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'delete user';
        $auth->add($deleteUser);

        $admin = $auth->getRole('admin');
        $auth->addChild($admin, $deleteRating);
        $auth->addChild($admin, $updateRating);
        $auth->addChild($admin, $viewPlace);
        $auth->addChild($admin, $createHall);
        $auth->addChild($admin, $deleteHall);
        $auth->addChild($admin, $updateHall);
        $auth->addChild($admin, $viewHall);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $viewUser);
        $auth->addChild($admin, $deleteUser);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->getRole('admin');
        $auth->removeChild($admin, $auth->getPermission('deleteRating'));
        $auth->removeChild($admin, $auth->getPermission('updateRating'));
        $auth->removeChild($admin, $auth->getPermission('viewPlace'));
        $auth->removeChild($admin, $auth->getPermission('createHall'));
        $auth->removeChild($admin, $auth->getPermission('deleteHall'));
        $auth->removeChild($admin, $auth->getPermission('updateHall'));
        $auth->removeChild($admin, $auth->getPermission('viewHall'));
        $auth->removeChild($admin, $auth->getPermission('createUser'));
        $auth->removeChild($admin, $auth->getPermission('updateUser'));
        $auth->removeChild($admin, $auth->getPermission('viewUser'));
        $auth->removeChild($admin, $auth->getPermission('deleteUser'));
    }
}
