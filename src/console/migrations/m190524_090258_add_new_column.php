<?php

use yii\db\Migration;

/**
 * Class m190524_090258_add_new_column
 */
class m190524_090258_add_new_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('review', 'rating', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('review', 'rating');
    }
}
