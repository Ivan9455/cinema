<?php

use yii\db\Migration;

/**
 * Class m190522_130209_rename_column
 */
class m190522_130209_rename_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('rating', 'film_id', 'review_id');
        $this->dropForeignKey(
            'fk-rating-film_id',
            'rating'
        );

        $this->addForeignKey(
            'fk-rating-review_id',
            'rating',
            'review_id',
            'review',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('rating', 'review_id', 'film_id');
        $this->dropForeignKey(
            'fk-rating-review_id',
            'rating'
        );

        $this->addForeignKey(
            'fk-rating-film_id',
            'rating',
            'film_id',
            'film',
            'id',
            'CASCADE'
        );
    }
}
