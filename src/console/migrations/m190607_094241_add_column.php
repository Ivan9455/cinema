<?php

use yii\db\Migration;

/**
 * Class m190607_094241_add_column
 */
class m190607_094241_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ticket', 'ics_name', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ticket', 'ics_name');
    }
}
