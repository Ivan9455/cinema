<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%review}}`.
 */
class m190502_100643_create_review_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%review}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'title' => $this->text()->notNull(),
            'content' => $this->text()->notNull(),
            'film_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk-review-user_id',
            'review',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-review-film_id',
            'review',
            'film_id',
            'film',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-post-user_id',
            'review'
        );

        $this->dropForeignKey(
            'fk-post-film_id',
            'review'
        );

        $this->dropTable('{{%review}}');
    }
}
