<?php

use yii\db\Migration;

/**
 * Class m190605_063537_add_permission_for_user
 */
class m190605_063537_add_permission_for_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        Yii::$app->authManager->addChild(
            Yii::$app->authManager->getRole('user'),
            Yii::$app->authManager->getPermission('deleteReservationTicket')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->authManager->removeChild(
            Yii::$app->authManager->getRole('user'),
            Yii::$app->authManager->getPermission('deleteReservationTicket')
        );
    }
}
