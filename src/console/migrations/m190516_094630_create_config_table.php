<?php

use yii\db\Migration;

/**
 * Handles the creation of table `config`.
 */
class m190516_094630_create_config_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('config', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->unique()->notNull(),
            'value' => $this->text()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('config');
    }
}
