<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%place}}`.
 */
class m190502_092841_create_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%place}}', [
            'id' => $this->primaryKey(),
            'hall_id' => $this->integer()->notNull(),
            'row' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull(),
            'rate' => $this->float()->notNull()
        ]);

        $this->addForeignKey(
            'fk-place-hall_id',
            'place',
            'hall_id',
            'hall',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-place-hall_id',
            'place'
        );

        $this->dropTable('{{%place}}');
    }
}
