<?php

use yii\db\Migration;

/**
 * Class m190530_120023_add_column
 */
class m190530_120023_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('ticket', 'reservation_code', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('ticket', 'reservation_code');
    }
}
