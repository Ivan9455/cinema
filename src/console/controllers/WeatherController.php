<?php

namespace console\controllers;

use yii\console\Controller;
use common\models\Weather;
use common\models\Config;
use Yii;

class WeatherController extends Controller
{
    public function actionIndex()
    {
        $config = Config::getValue(Weather::KEY_CITY);
        if (isset($config)) {
            $weather = new Weather();
            $config = unserialize($config);
            $result = $weather->getInfo($config['url']);
            $result['city'] = urldecode($config['city']);
            Yii::$app->cache->set(Weather::KEY_CITY, $result, Weather::CACHE_DURATION);
        }
    }
}
