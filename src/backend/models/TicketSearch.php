<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Ticket;

class TicketSearch extends Ticket
{
    public $userName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'seance_id', 'place_id', 'price'], 'integer'],
            [['userName'], 'string'],
            [['status', 'token'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ticket::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'token', $this->token])
            ->andFilterWhere([
                'ticket.id' => $this->id,
                'seance_id' => $this->seance_id,
                'place_id' => $this->place_id,
                'price' => $this->price,
            ]);

        $query->innerJoin('user', 'user_id = user.id')
            ->andFilterWhere(['like', 'username', $this->userName]);

        return $dataProvider;
    }
}
