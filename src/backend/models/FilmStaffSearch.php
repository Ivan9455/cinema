<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FilmStaff;

class FilmStaffSearch extends FilmStaff
{
    public $staffName;
    public $filmName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['staffName', 'filmName'], 'string'],
            [['role'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FilmStaff::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'film_staff.id' => $this->id,
            'film_staff.role' => $this->role,
        ]);

        $query->innerJoin('staff', 'staff_id = staff.id')
            ->andFilterWhere(['like', 'staff.name', $this->staffName])
            ->innerJoin('film', 'film_id = film.id')
            ->andFilterWhere(['like', 'film.title', $this->filmName]);

        return $dataProvider;
    }
}
