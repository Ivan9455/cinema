<?php

namespace backend\models;

use common\models\FilmStaff;
use yii\base\Model;
use common\models\Film;
use common\models\Staff;
use Yii;

class FilmForm extends Model
{
    public $title;
    public $description;
    public $time;
    public $country;
    public $img;
    public $staff_id;
    public $staff;
    public $film_id;
    public $release_date;
    public $rating_kinopoisk;
    public $rating_imdb;
    public $rating_rotten_tomatoes;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'time', 'country', 'staff_id', 'release_date'], 'required'],
            [['title', 'description'], 'string'],
            [['time'], 'safe'],
            [['staff_id'], 'integer'],
            [['country'], 'string', 'max' => 64],
            [['img'], 'file', 'extensions' => 'png, jpg'],
            [['staff'], 'validateStaff'],
            [['rating_kinopoisk', 'rating_imdb', 'rating_rotten_tomatoes'], 'double'],
        ];
    }

    public function validateStaff($attribute)
    {
        if (!isset($this->staff)) {
            $this->addError($attribute, 'Add actors role !');
        }
    }

    /**
     * @return bool
     */
    public function saveStaff() : bool
    {
        foreach ($this->staff as $item => $value) {
            $staff = new FilmStaff();
            $staff->film_id = $this->film_id;
            $staff->staff_id = $item;
            $staff->role = $value;

            if (empty($item) || empty($value) || !$staff->save()) {
                Yii::$app->session->setFlash('error', 'Failed to save Actor');

                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function saveFilm() : bool
    {
        $film = new Film();
        $film->setAttributes($this->getAttributes());

        if ($film->save()) {
            $this->film_id = $film->id;

            return true;
        }

        Yii::$app->session->setFlash('error', 'Failed to save Film');

        return false;
    }

    /**
     * @param $file
     *
     * @return bool
     */
    public function upload($file) : bool
    {
        if ($file->saveAs(Yii::getAlias('@frontend') . '/web/uploads/' . $file->name)) {
            return true;
        }

        return false;
    }
}
