<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rating;

/**
 * RatingSearch represents the model behind the search form of `common\models\Rating`.
 */
class RatingSearch extends Rating
{
    public $filmName;
    public $userName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'value', 'review_id'], 'integer'],
            [['userName'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rating::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'value' => $this->value,
        ]);

        $query->innerJoin('user', 'user_id = user.id')
            ->andFilterWhere(['like', 'username', $this->userName]);

        $query->innerJoin('review', 'review_id = review.id')
            ->andFilterWhere(['like', 'review.title', $this->filmName]);

        return $dataProvider;
    }
}
