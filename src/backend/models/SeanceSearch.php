<?php

namespace backend\models;

use common\models\Film;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Seance;

/**
 * SeanceSearch represents the model behind the search form of `common\models\Seance`.
 */
class SeanceSearch extends Seance
{
    public $filmName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'price', 'hall_id', 'film_id'], 'integer'],
            [['filmName'], 'string'],
            [['start', 'end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Seance::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'seance.id' => $this->id,
            'price' => $this->price,
            'hall_id' => $this->hall_id,
            'start' => $this->start,
            'end' => $this->end,
        ]);

        $query->innerJoin('film', 'film_id = film.id')
            ->andFilterWhere(['like', 'film.title', $this->filmName]);

        return $dataProvider;
    }
}
