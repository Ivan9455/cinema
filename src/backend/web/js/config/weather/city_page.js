$(document).ready(function () {
    $('.cities').on('click', '.city', function () {
        City.save(this.dataset.city, this.dataset.url).then(function (result) {
            if (result) {
                alert('city successful save/update');
            } else {
                alert('city error save/update');
            }
        });
    });
    $('.load').click(function () {
        City.load($('.search_title')[0].value).then(function (result) {
            $('.cities')[0].innerHTML = result;
        })
    })
});
