let City = {
    save: function (city_name, url) {
        return $.ajax({
            type: "POST",
            url: "/config/city/save",
            data: {
                _csrf: $('form').serializeArray()['_csrf-backend'],
                city: city_name,
                url: url
            }
        });
    },
    load: function () {
        return $.ajax({
            type: "POST",
            url: "/config/city/search",
            data: $('form').serialize()
        })
    }
};
