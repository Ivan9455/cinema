$('#filmform-staff_id').change(function () {
    Staff.addForm($('#filmform-staff_id').val(), $('#filmform-staff_id option:selected').text());
});

var Staff = {

    addForm : function (staff_id, name) {
        for(var i = 0; i <= staff_id.length-1; i++){
            $('.staff-role').append(
                '<div class="form-group field-filmstaff-role role">' +
                    '<label class="control-label" for="filmstaff-role">Role ' + name + '</label>' +
                    '<input type="text" id="filmstaff-role" class="form-control" name="FilmForm[staff][' + staff_id[i] + ']" maxlength="255" aria-invalid="false">' +
                    '<div class="help-block"></div>' +
                '</div>'
            );
        }
    }
};
