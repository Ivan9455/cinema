<?php

namespace backend\assets\config\weather;

use yii\web\AssetBundle;

class CityAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/config/weather/city_page.js',
        'js/config/weather/City.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
