<?php

use yii\helpers\Html;
use common\models\Film;

/* @var $this yii\web\View */
/* @var int $countFilm */
/* @var int $countSeanse */
/* @var int $countUser */

$this->title = 'Stats';
?>
<div class="site-index">
    <div class="list-group">
        <div class="stats">
            <div class="stat_user">
                <span class="user_icon"><i class="far fa-user"></i></span>
                <div class="stat_description">
                    <p>Количество зарегистрированных пользователей : <?= $countUser ?></p>
                </div>
            </div>

            <div class="stat_film">
                <span class="film_icon"><i class="fa fa-film" aria-hidden="true"></i></span>
                <div class="stat_description">
                    <p>Количество фильмов : <?= $countFilm ?></p>
                </div>
            </div>

            <div class="stat_seanse">
                <span class="seance_icon"><i class="fa fa-table" aria-hidden="true"></i></span>
                <div class="stat_description">
                    <p>Количество сеансов : <?= $countSeanse ?></p>
                </div>
            </div>
        </div>
        <h3>Cache</h3>
        <div class="cache-info">
            <a href="/site/cache" title="Clear cache" class="cache" aria-label="Delete" data-pjax="0"
               data-confirm="Are you sure you want to clear cache?" data-method="post">
                Clear cache
            </a>
        </div>
    </div>
</div>
