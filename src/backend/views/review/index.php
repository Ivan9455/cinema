<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReviewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-index">
    <p>
        <?= Html::a('Create Review', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'User',
                'attribute' => 'userName',
                'value' => function($data){
                    return $data->getUser($data->user_id)->one()->username;
                }
            ],
            'title:ntext',
            'content:ntext',
            [
                'label' => 'Film',
                'attribute' => 'filmName',
                'value' => function($data){
                    return $data->getFilm($data->film_id)->one()->title;
                }
            ],
            'rating',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
