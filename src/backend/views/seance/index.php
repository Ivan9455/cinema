<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Hall;
use yii\helpers\ArrayHelper;
use common\models\Film;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SeanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seances';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seance-index">

    <p>
        <?= Html::a('Create Seance', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'price',
            [
                'attribute' => 'hall_id',
                'label' => 'Hall',
                'filter' => ArrayHelper::map(Hall::find()->all(), 'id', 'number')
            ],
            [
                'attribute' => 'filmName',
                'label' => 'Film name',
                'value' => function($data){
                    return $data->getFilm($data->film_id)->one()->title;
                },
            ],
            'start',
            'end',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
