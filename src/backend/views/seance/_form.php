<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Seance */
/* @var $form yii\widgets\ActiveForm */
/* @var array $hall common\models\Hall */
/* @var array $film common\models\Film */
?>

<div class="seance-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'film_id')->dropDownList(
        ArrayHelper::map($film, 'id', 'title'),
        [
            'prompt' => 'Select film...',
            'value' => ''
        ]
    )->label('Film') ?>

    <?= $form->field($model, 'hall_id')->dropDownList(
        ArrayHelper::map($hall, 'id', 'number'),
        [
            'prompt' => 'Select hall...',
            'value' => ''
        ]
    )->label('Hall') ?>

    <?= $form->field($model, 'start')->input('datetime-local') ?>

    <?= $form->field($model, 'end')->input('datetime-local') ?>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
