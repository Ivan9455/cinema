<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\FilmStaffSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Film Staff';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="film-staff-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'Film',
                'attribute' => 'filmName',
                'value' => function($data){
                    return $data->getFilm($data->film_id)->one()->title;
                }
            ],
            [
                'label' => 'Staff',
                'attribute' => 'staffName',
                'value' => function($data){
                    $staff = $data->getStaff($data->staff_id)->one();

                    return $staff->name . ' ' . $staff->surname;
                }
            ],
            'role',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
