<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tickets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'label' => 'User',
                'attribute' => 'userName',
                'value' => function($data){
                    return $data->getUser($data->user_id)->one()->username;
                }
            ],
            'seance_id',
            'place_id',
            'status',
            'price',
            'token',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
