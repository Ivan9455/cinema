<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\FilmStaff;

/* @var $this yii\web\View */
/* @var $model common\models\Film */
/* @var $form yii\widgets\ActiveForm */
/* @var array $staff common\models\Staff */
?>

<div class="film-form">

    <?php $form = ActiveForm::begin([
        'id' => 'film-staff'
    ]); ?>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'time')->input('time') ?>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'release_date')->input('date') ?>

    <?= $form->field($model, 'rating_imdb')->textInput() ?>

    <?= $form->field($model, 'rating_kinopoisk')->textInput() ?>

    <?= $form->field($model, 'rating_rotten_tomatoes')->textInput() ?>

    <?= $form->field($model, 'img')->fileInput() ?>

    <div class="preview">
        <p>Preview</p>
        <img id="image_upload_preview" src="" alt="your image"/>
    </div>

    <div class="add-staff">
        <hr>
        <?php if (!empty($staff)): ?>
            <div class="staff-select">
                <?= $form->field($model, 'staff_id')->dropDownList(
                    ArrayHelper::map($staff, 'id', 'fullName'),
                    [
                        'prompt' => 'Select actor...',
                        'value' => ''
                    ]
                ) ?>

                <div class="staff-role"></div>

                <button class="add btn btn-success">Add staff</button>
            </div>
        <?php endif; ?>
        <a href="/staff/create" class="btn btn-warning">Add new actor</a>
    </div>
    <hr>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn__film btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
