<?php if (isset($error)): ?>
    <div id="w0-error" class="alert-danger alert fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="icon fa fa-ban"></i><?= $error ?>
    </div>
<?php endif; ?>
<?php if (isset($weather['cities'], $weather['response']) && $weather['response'] === true): ?>
    <div class="list-group">
        <?php foreach ($weather['cities'] as $city) : ?>
            <a href="#" class="list-group-item city" data-url="<?= $city['url'] ?>" data-city="<?= $city['city'] ?>">
                <?= $city['area'] ?>
            </a>
        <?php endforeach; ?>
    </div>
<?php endif ?>
