<?php

use backend\assets\config\weather\CityAsset;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $weather array
 */

$this->title = 'Select city';
$this->params['breadcrumbs'][] = ['label' => 'Config', 'url' => ['/config']];;
$this->params['breadcrumbs'][] = ['label' => 'Weather', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
CityAsset::register($this);
?>
<?= Html::beginForm(); ?>
<?= Html::input('search', 'city', null, ['class' => 'btn search_title']); ?>
<?= Html::input('button', 'go', 'go', ['class' => 'btn btn-primary load']); ?>
<?= Html::endForm(); ?>
<div class="cities"></div>
