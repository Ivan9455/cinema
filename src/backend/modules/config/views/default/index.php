<?php
/**
 * @var $this \yii\web\View
 */
$this->title = 'Config';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
?>

<div class="list-group">
    <a href="#" class="list-group-item">
        <span class="glyphicon glyphicon-cloud"></span>Weather
    </a>
    <div class="list-group-item">
        <a href="config/city" class="list-group-item">City</a>
    </div>
</div>
