<?php

namespace backend\modules\config\controllers;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\Weather;
use common\models\Config;
use yii\web\Controller;
use Yii;

class CityController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search'],
                        'roles' => ['viewConfigWeatherCity'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['save'],
                        'roles' => ['saveConfigWeatherCity'],
                    ],
                ]
            ]
        ];
    }

    /**
     * @return string
     */
    public function actionIndex() : string
    {
        return $this->render('index');
    }

    /**
     * @return bool
     */
    public function actionSave() : bool
    {
        if (Yii::$app->request->isAjax) {
            if (!empty(Yii::$app->request->post('city')) && !empty(Yii::$app->request->post('url'))) {
                if (Config::getValue(Weather::KEY_CITY) === null) {
                    $config = new Config();
                    $config->key = Weather::KEY_CITY;
                } else {
                    $config = Config::findOne(['key' => Weather::KEY_CITY]);
                }
                $config->value = serialize([
                    'city' => urlencode(Yii::$app->request->post('city')),
                    'url' => Yii::$app->request->post('url')
                ]);

                return $config->save();
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function actionSearch() : string
    {
        if (Yii::$app->request->isAjax) {
            if (!empty(Yii::$app->request->post('city'))) {
                $weather = new Weather();
                $result = $weather->getSearchCities(strtolower(Yii::$app->request->post('city')));

                return $this->renderAjax('_cities', ['weather' => $result, 'error' => $result['error'] ?? null]);
            }
        }

        return $this->renderAjax('_cities', ['error' => 'sorry not found...']);
    }
}
