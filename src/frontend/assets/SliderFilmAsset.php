<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class SliderFilmAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/slider_film.css',
    ];
}
