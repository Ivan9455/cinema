<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var $seance \common\models\Seance */
/** @var $ticket \common\models\Ticket */

$this->title = $seance->film->title;
$this->params['breadcrumbs'][] = ['label' => 'Seance', 'url' => ['/seance']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<?= DetailView::widget([
    'model' => $seance,
    'attributes' => [
        [
            'attribute' => 'img',
            'value' => '/uploads/' . $seance->film->img,
            'format' => ['image', ['width' => '200', 'height' => '350']],
        ],
        [
            'attribute' => 'description',
            'value' => $seance->film->description,
        ],
        [
            'attribute' => 'time',
            'value' => $seance->film->time,
        ],
        [
            'attribute' => 'country',
            'value' => $seance->film->country,
        ],
        [
            'attribute' => 'rating_kinopoisk',
            'value' => $seance->film->rating_kinopoisk,
        ],
        [
            'attribute' => 'rating_imdb',
            'value' => $seance->film->rating_imdb,
        ],
        [
            'attribute' => 'rating_rotten_tomatoes',
            'value' => $seance->film->rating_rotten_tomatoes,
        ],
    ],
]) ?>


<?php $form = ActiveForm::begin([
    'action' => '/ticket/reservation'
]) ?>
    <?= $form->field($ticket, 'place_id[]')
            ->dropDownList($seance->getPrice(), ['multiple' => 'multiple'])
            ->label('Place'); ?>
    
    <?= $form->field($ticket, 'seance_id')->hiddenInput(['value' => $seance->id])->label(false) ?>

    <?= Html::submitButton('Ticket reservation', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
<?php $form->end(); ?>
