<?php

use yii\helpers\Html;

/* @var array $tickets frontend\models\Ticket */

$this->title = 'My ticket';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (empty($tickets)): ?>
        <div id="w2-error-0" class="alert-danger alert fade in">
            Nothing tickets :(
        </div>
    <?php endif; ?>

    <?php foreach ($tickets as $ticket): ?>
        <div class="tickets">
            <div class="tickets-item__title">
                <a class="badge badge-pill badge-primary" href="/film/<?= $ticket->seance->film->id ?>">
                    <?= $ticket->seance->film->title ?>
                </a>
            </div>
            <div class="tickets-item__film__img">
                <img src="/uploads/<?= $ticket->seance->film->img ?>" alt="">
            </div>
            <div class="ticket-info">
                <div class="ticket-info__hall">
                    <p class="badge badge-pill badge-primary r-color">
                        Hall : <?= $ticket->seance->hall->number ?>
                    </p>
                    <p class="badge badge-pill badge-primary r-color">
                        Start : <?= Yii::$app->formatter->asDate($ticket->seance->start, 'yyyy-MM-dd HH:i') ?>
                    </p>
                </div>
                <div class="ticket-info__place">
                    <p class="badge badge-pill badge-primary r-color">Row-Place :</p>
                    <?php foreach ($ticket->places as $place): ?>
                        <span class="badge badge-success"><?= $place->row ?>-<?= $place->position ?></span>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="ticket-control">
                <div class="ticket-control__price">
                    <p class="badge badge-pill badge-primary r-color">
                        Price : <?= number_format($ticket->getFinalPrice(), 2) ?>
                        <i class="fas fa-ruble-sign"></i>
                    </p>
                </div>
                <div class="ticket-control__number-reservation">
                    <p class="badge badge-pill badge-success g-color">#<?= $ticket->reservation_code ?></p>
                </div>
                <a href="/ticket/delete?id=<?= $ticket->id ?>" class="ticket-close__reservation"
                   title="Close reservation" aria-label="Close reservation" data-pjax="0"
                   data-confirm="Are you sure you want to close reservation" data-method="post">
                    <i class="fas fa-times"></i>
                </a>
            </div>
        </div>
    <?php endforeach; ?>
</div>
