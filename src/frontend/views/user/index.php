<?php

use yii\helpers\Html;

?>

<div class="user-index">

    <div class="list-group">
        <h2>Name :<?= Yii::$app->user->identity->username; ?></h2>
        <h2>Email :<?= Yii::$app->user->identity->email; ?></h2>
        <a href="/seance" class="list-group-item list-group-item-action">
            Seance today
        </a>
        <a href="/user/ticket" class="list-group-item list-group-item-action">
            My ticket
        </a>
        <?php echo '<a>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'list-group-item list-group-item list-group-item-action list-group-item-danger']
            )
            . Html::endForm()
            . '</a>'
        ?>
    </div>

</div>
