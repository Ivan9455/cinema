<?php
/* @var array $film common\models\Film */
/* @var $date common\models\traits\Date; */
?>

<div class="seance">
    <?php if (empty($film)):  ?>
        <div id="w2-error-0" class="alert-danger alert fade in">
            Nothing on this date :(
        </div>
    <?php endif; ?>

    <?php foreach ($film as $item): ?>
        <div class="seance-item">
            <div class="seance-item__title">
                <a class="badge badge-pill badge-primary" href="/film/<?= $item->id ?>">
                    <?= $item->title ?>
                </a>
            </div>
            <div class="seance-item__film__img">
                <a href="/film/<?= $item->id ?>">
                    <img src="/uploads/<?= $item->img ?>" alt="">
                </a>
            </div>
            <div class="schedule">
                <?php foreach ($item->getCurrentSeances(Yii::$app->request->get('date')) as $seance): ?>
                    <div class="schedule__item">
                        <a href="/seance/<?= $seance->id ?>" class="schedule__item-time">
                            <p class="badge badge-pill badge-primary">
                                <?= Yii::$app->formatter->asDate($seance->start, 'HH:i') ?>
                                -
                                <?= Yii::$app->formatter->asDate($seance->end, 'HH:i') ?>
                            </p>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>
