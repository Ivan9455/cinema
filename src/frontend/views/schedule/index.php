<?php

use yii\helpers\Html;

/* @var array $film common\models\Seance */
/* @var $date common\models\traits\Date; */

$this->title = 'Schedule';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1>
    <?= Html::encode($this->title) ?>
    <?= Html::a(
        'Today',
        ['/schedule?date=' . $date['today']],
        ['class' => 'badge badge-pill badge-primary r-color']
    ) ?>
    <?= Html::a(
        'Tomorrow',
        ['/schedule?date=' . $date['tomorrow']],
        ['class' => 'badge badge-pill badge-primary r-color']
    ) ?>
    <?= Html::a(
        'Day after tomorrow',
        ['/schedule?date=' . $date['afterTomorrow']],
        ['class' => 'badge badge-pill badge-primary r-color']
    ) ?>
</h1>
<hr>
<?= $this->render('view', [
    'film' => $film,
    'date' => $date,
]) ?>
