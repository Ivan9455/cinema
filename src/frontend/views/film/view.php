<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Film */
/* @var $modelReview common\models\Review */


$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Films', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="film-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'img',
                'value' => '/uploads/' . $model->img,
                'format' => ['image',['width'=>'200','height'=>'350']],
            ],
            'description:ntext',
            'time',
            'country',
            'release_date',
            'rating_kinopoisk',
            'rating_imdb',
            'rating_rotten_tomatoes',
        ],
    ]) ?>

    <?php foreach ($model->getReviews()->all() as $review):  ?>
        <?php if($review->rating > 3): ?>
            <div class="review alert-success alert">
        <?php else: ?>
            <div class="review alert-danger alert">
        <?php endif; ?>
            <div class="review__title"><?= $review->title ?></div>
            <div class="review__content"><?= $review->content ?></div>
            <hr>
            <div class="review__user"><?= $review->user->username ?></div>
            <div class="review__rating">
                <i class="far fa-star"></i>
                <?= $review->rating ?>
            </div>
            <?php if (Yii::$app->authManager->checkAccess(Yii::$app->user->identity->id, 'deleteReview')):  ?>
                <div class="review__delete">
                    <a href="/review/delete/<?= $review->id ?>" title="Delete" aria-label="Delete" data-pjax="0" data-confirm="Are you sure you want to delete this review?" data-method="post">
                        <i class="fas fa-times"></i>
                    </a>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>

    <hr>

    <div class="review-form">
        <h3>Add new review</h3>
        <?php if(!Yii::$app->user->isGuest): ?>
            <?php $form = ActiveForm::begin([
                'action' => '/review/create'
            ]); ?>

            <?= $form->field($modelReview, 'title')->textInput() ?>

            <?= $form->field($modelReview, 'content')->textarea(['rows' => 6]) ?>

            <?= $form->field($modelReview, 'film_id')->hiddenInput(['value' => $model->id])->label(false) ?>

            <?= $form->field($modelReview, 'rating')->input('range', ['min' => 1, 'max' => 5]) ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        <?php else: ?>
            <p><a href="/site/login">Войдите</a> или <a href="/site/signup">зарегистрируйтесь</a></p>
        <?php endif; ?>
    </div>
</div>
