<?php

use yii\helpers\Html;
use common\models\Staff;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\StaffSearch */
/* @var array $staff frontend\models\Staff */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Staff';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staff-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($staff as $staff):  ?>
        <div class="actor">
            <a class="badge badge-pill badge-primary" href="/staff/<?= $staff->id ?>">
                <?= $staff->name ?> <?= $staff->surname ?>
            </a>
        </div>
    <?php endforeach; ?>
</div>
