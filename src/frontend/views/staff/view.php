<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Staff */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Staff', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="staff-view">

    <h1><?= Html::encode($this->title . ' ' . $model->surname) ?></h1>
    <hr>
    <div class="films">
        <?php foreach ($model->getFilmStaff()->all() as $item): ?>
            <div class="film">
                <div class="film__title">
                    <a class="badge badge-pill badge-primary" href="/film/<?= $item->getFilm()->one()->id ?>">
                        <?= $item->getFilm()->one()->title ?>
                    </a>
                </div>
                <div class="film__img">
                    <img src="/uploads/<?= $item->getFilm()->one()->img ?>" alt="">
                </div>
                <div class="film__staff-position">
                    <i class="fas fa-user-tag"></i>
                    <?= $item->role ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
