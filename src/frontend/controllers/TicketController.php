<?php

namespace frontend\controllers;

use common\models\Config;
use common\models\Place;
use common\models\Ticket;
use common\models\traits\Date;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use Yii;

class TicketController extends Controller
{
    use Date;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['reservation'],
                        'roles' => ['reservationTicket'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteReservationTicket'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     * @throws \yii\base\Exception
     */
    public function actionReservation()
    {
        if (Yii::$app->request->post()) {
            if (Ticket::isExist(ArrayHelper::getValue(Yii::$app->request->post(), 'Ticket.seance_id'))) {
                Yii::$app->session->setFlash('error', 'Sorry, you have already booked');

                return $this->redirect(Yii::$app->request->referrer);
            }

            $places = ArrayHelper::getValue(Yii::$app->request->post(), 'Ticket.place_id');

            if (count($places) > Config::getValue('ticket/restriction')) {
                Yii::$app->session->setFlash('error', 'Sorry, cannot book more than 5 tickets');

                return $this->redirect(Yii::$app->request->referrer);
            }

            $reservationCode = Yii::$app->security->generateRandomString(6);
            $icsName = Yii::$app->security->generateRandomString(36);

            $ticket = null;

            foreach ($places as $place_id) {
                $model = new Ticket();
                $model->load(Yii::$app->request->post());
                $model->status = Ticket::STATUS_RESERVATION;
                $model->price = $model->seance->price * Place::findOne($place_id)->rate;
                $model->place_id = $place_id;
                $model->reservation_code = $reservationCode;
                $model->ics_name = $icsName;
                $ticket = $model;

                if (!$model->save()) {
                    throw new BadRequestHttpException('Sorry, failed to save ticket');
                }
            }

            $ticket->trigger(Ticket::SEND_MAIL);
            Yii::$app->session->setFlash('success', 'Tickets successfully reservation :)');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Ticket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        Ticket::deleteAll([
            'user_id' => $model->user_id,
            'seance_id' => $model->seance_id,
            'reservation_code' => $model->reservation_code
        ]);

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Ticket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Ticket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
