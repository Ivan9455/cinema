<?php

namespace frontend\controllers;

use common\models\Film;
use common\models\Seance;
use common\models\Ticket;
use common\models\traits\Date;
use yii\web\NotFoundHttpException;

class SeanceController extends \yii\web\Controller
{
    use Date;

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'film' => Seance::getCurrentFilms(),
        ]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView(int $id)
    {
        $seance = Seance::find()->where(['id' => $id])->andWhere(
            ['>', 'seance.start', $this->getDate('today')]
        )->one();
        if (!isset($seance)) {
            throw new NotFoundHttpException('page not found');
        }

        return $this->render('view', [
            'seance' => $seance,
            'ticket' => new Ticket()
        ]);
    }
}
