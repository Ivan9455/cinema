<?php

namespace frontend\controllers;

use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\Review;
use Yii;

class ReviewController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'create' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => ['createReview'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteReview'],
                    ],
                ],
                'denyCallback' => function () {
                    Yii::$app->session->setFlash('error', 'Sorry, you dont have permission');

                    $this->redirect(Yii::$app->request->referrer);
                }
            ]
        ];
    }

    /**
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionCreate()
    {
        $model = new Review();

        if(!$model->load(Yii::$app->request->post()) || !$model->save()){
            Yii::$app->session->setFlash('error', 'Sorry, failed to save review');
        }

        return $this->redirect(['/film/' . $model->film_id]);
    }

    /**
     * Deletes an existing Review model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Review model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Review the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Review::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
