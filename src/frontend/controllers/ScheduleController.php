<?php

namespace frontend\controllers;

use common\models\Film;
use yii\web\Controller;
use common\models\traits\Date;
use common\models\Seance;
use DateTime;
use Yii;

class ScheduleController extends Controller
{
    use Date;

    public function actionIndex(string $date)
    {
        if (!DateTime::createFromFormat('Y-m-d', $date)) {
            Yii::$app->session->setFlash('error', 'Nothing on this date :(');

            return $this->redirect(['/schedule?date=' . $this->getDate('today')]);
        }

        return $this->render('index', [
            'film' => Seance::getCurrentFilms($date),
            'date' => [
                'today' => $this->getDate('today'),
                'tomorrow' => $this->getDate('tomorrow'),
                'afterTomorrow' => $this->getDate('+2 day')
            ]
        ]);
    }
}
