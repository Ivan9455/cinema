<?php
/* @var $ticket frontend\models\Ticket */
?>
<div class="staff-index">
    <article class="ticket">
        <header class="ticket__wrapper">
            <div class="ticket__header">
                <?= $ticket->seance->film->title ?>
            </div>
        </header>
        <div class="ticket__divider">
            <div class="ticket__notch"></div>
            <div class="ticket__notch ticket__notch--right"></div>
        </div>
        <div class="ticket__body">
            <section class="ticket__section">
                <h3>Your Tickets</h3>
                <span>Code: <p class="badge r-color"><?= $ticket->reservation_code ?></p></span>
                <span>Row-place:
                    <?php foreach ($ticket->user->getReservationPlaces($ticket->seance_id, $ticket->reservation_code)['places'] as $place): ?>
                        <p class="badge"><?= $place->row ?>-<?= $place->position ?></p>
                    <?php endforeach; ?>
                </span>
                <span>
                    Hall:
                    <p class="badge"><?= $ticket->seance->hall->number ?></p>
                </span>
            </section>
            <section class="ticket__section">
                <h3>Start seance</h3>
                <p class="badge"><?= Yii::$app->formatter->asDate($ticket->seance->start, 'yyyy-MM-dd HH:i') ?></p>
            </section>
        </div>
        <footer class="ticket__footer">
            <span>Total price</span>
            <span><?= number_format($ticket->getFinalPrice(), 2) ?></span>
        </footer>
    </article>
</div>
